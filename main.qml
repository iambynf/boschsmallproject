import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import BoschPremium 1.0

Window {
    width: 1000
    height: 500
    visible: true
    title: qsTr("Bosch Premium")
    color: "black"
    Item {
        Keys.onPressed: (event)=> {
            if (event.key === Qt.Key_F1) {
                stackView.pop(null)
                mainController.setHeader("")
                status.visible = true
                event.accepted = true
            }
        }
        focus: true
    }

    MainController {
        id: mainController
    }

    Connections {
        target: mainController
        function onHeaderChanged() { header.text = mainController.getHeader()}
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: "Home.qml"
    }

    Text {
        id: header
        text: mainController.getHeader()
        color: "darkgray"
        font.pixelSize: 32
        anchors {
            top: parent.top
            topMargin: 20
            left: parent.left
            leftMargin: 20
        }
    }

    Status {
        id: status
        anchors {
            top: parent.top
            topMargin: 20
            right: parent.right
        }
    }
}
