import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import BoschPremium 1.0
Item {
    ListModel {
        id: listModel
        ListElement { name : "Types of\nheating"
                      sub: "Manual preparation"
                      prog: "qrc:/Heating.qml"
                      src: "qrc:/Assets/heating.jpg"}
        ListElement { name : "Microwave"
                      sub: "Quick preparation"
                      prog: "qrc:/Microwave.qml"
                      src: "qrc:/Assets/microwave.jpg"}
        ListElement { name : "Steam"
                      sub: "Gentle preparation"
                      prog: "qrc:/Steam.qml"
                      src: "qrc:/Assets/steam.jpg"}
        ListElement { name : "Meals"
                      sub: "Assist programmes"
                      prog: "qrc:/Meals.qml"
                      src: "qrc:/Assets/Meals.jpg"}
        ListElement { name : "Favourite"
                      sub: ""
                      prog: "qrc:/Favourite.qml"
                      src: "qrc:/Assets/Favourite.jpg"}
        ListElement { name : "Cleaning"
                      sub: ""
                      prog: "qrc:/Cleaning.qml"
                      src: "qrc:/Assets/Cleaning.jpg"}
        ListElement { name : "Basic settings"
                      sub: ""
                      prog: "qrc:/BasicSettings.qml"
                      src: "qrc:/Assets/setting.png"}
    }

    Component {
        id: itemDelegate
        Rectangle {
            width: ListView.view.width
            height: ListView.view.height
            color: "#00FFFFFF"
            Image {
                id: image
                width: 299.63
                height: parent.height
                source: model.src
            }
            Text {
                id: text
                anchors.left: image.right
                text: model.name
                color: "white"
                font.pixelSize: 48
                font.bold: true
                anchors {
                    leftMargin: 24
                    verticalCenter: parent.verticalCenter
                }
            }
            Text {
                color: "gray"
                font.pixelSize: 20
                text: model.sub
                anchors {
                    left: image.right
                    leftMargin: 24
                    top: text.bottom
                    verticalCenter: parent.verticalCenter
                }
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(model.name === "Types of\nheating") {
                        mainController.setHeader("Types of heating")
                    } else {
                        mainController.setHeader(model.name)
                    }
                    stackView.push(model.prog)
                    if(model.prog === "qrc:/Favourite.qml"){
                        status.visible = false
                    }
                }
            }
        }

    }

    ListView {
            id: listView
            width: 600
            height: 200
            snapMode: ListView.SnapPosition
            orientation: ListView.Horizontal
            highlightRangeMode: ListView.StrictlyEnforceRange
            focus: true

            model: listModel

            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }

            delegate: itemDelegate
        }

    PageIndicator {
        id: control
        currentIndex: listView.currentIndex
        count: listView.count
        interactive: true
        delegate: Rectangle {
            implicitWidth: 16
            implicitHeight: 16
            radius: width / 2
            color: index === control.currentIndex? "red" : "gray"
            required property int index
        }
        anchors {
            top: listView.bottom
            topMargin: 30
            horizontalCenter: parent.horizontalCenter
        }
    }
}

