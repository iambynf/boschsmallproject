#include "maincontroller.h"

MainController::MainController(QObject *parent)
    : QObject{parent}
{

}

QString MainController::getHeader()
{
    return m_header;
}

QString MainController::header()
{
    return m_header;
}

void MainController::setHeader(QString newHeader)
{
    if (m_header == newHeader)
        return;
    m_header = newHeader;
    emit headerChanged();
}
