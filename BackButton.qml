import QtQuick 2.0
import QtQuick.Controls 2.15

Button {
    id: button
    Image {
        source: "qrc:/Assets/Back.png"
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
        width: 48
        height: 48
    }
    background: Rectangle {
        implicitWidth: 92
        implicitHeight: 64
        color: button.down ? "#d6d6d6" : "#f6f6f6"
        border.color: "#26282a"
        border.width: 1
        radius: 30
    }
    anchors {
        left: parent.left
        leftMargin: 20
        verticalCenter: parent.verticalCenter
    }
    onClicked:{
        stackView.pop()
        mainController.setHeader("")
    }
}
