#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>

class MainController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString header READ header WRITE setHeader NOTIFY headerChanged)
public:
    explicit MainController(QObject *parent = nullptr);
    Q_INVOKABLE QString getHeader();
    QString header();
public slots:
    void setHeader(QString);
signals:
    void headerChanged();
private:
    QString m_header;
};

#endif // MAINCONTROLLER_H
