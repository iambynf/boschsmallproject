import QtQuick 2.0
Item {
    BackButton {}
    Text {
        id: text
        text: qsTr("Basic settings")
        color: "darkgray"
        font.pixelSize: 80
        anchors.centerIn: parent
    }
}
