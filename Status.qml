import QtQuick 2.0
Rectangle {
    id: status
    width: 250
    height: 50
    color: "#00FFFFFF"
    Image {
        id: lock
        source: "qrc:/Assets/lock.png"
        anchors {
            right: wifi.left
            verticalCenter: parent.verticalCenter
        }
    }
    Image {
        id: wifi
        source: "qrc:/Assets/nowifi.png"
        anchors {
            right: sep.left
            rightMargin: 10
            verticalCenter: parent.verticalCenter
        }
    }
    Rectangle {
        id: sep
        width: 6
        height: 60
        color: "darkGray"
        smooth: true
        opacity: 0.2
        anchors {
                right: more.left
                rightMargin: 10
                verticalCenter: parent.verticalCenter
            }
    }
    Image {
        id: more
        source: "qrc:/Assets/more.png"
        anchors {
            right: parent.right
            topMargin: 10
            verticalCenter: parent.verticalCenter
        }
    }
}
