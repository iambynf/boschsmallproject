import QtQuick 2.0
Item {
    property bool isShowDialog: true
    Rectangle {
        id: dialog
        color: "black"
        anchors.fill: parent
        visible: isShowDialog
        Image {
            source: "qrc:/Assets/close.png"
            width: 48
            height: 48
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    status.visible = true
                    isShowDialog = false
                }
            }
            anchors {
                top: parent.top
                right: parent.right
                topMargin: 20
                rightMargin: 20
            }
        }
        Text {
            text: qsTr("You can save your preferred oven settings as favourites by
selecting the favourites symbol in the types of heating or
dishes. Alternatively, you can also create favourites in the
Home Connect app and save these on the appliance.")
            color: "white"
            anchors.centerIn: parent
            font.pixelSize: 32
        }
    }
    Rectangle {
        anchors.fill: parent
        color: "black"
        visible: !isShowDialog
        BackButton {}
        Text {
            id: text
            text: qsTr("Favourite")
            color: "darkgray"
            font.pixelSize: 80
            anchors.centerIn: parent
        }
    }
}
