import QtQuick 2.0

Item {
    Image {
        id: mainMenu
        source: "qrc:/Assets/main-menu.jpg"
        height: 200
        width: 700.73
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        MouseArea{
            anchors.fill: parent
            onClicked: stackView.push("Menu.qml")
        }
    }
}
