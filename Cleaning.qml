import QtQuick 2.0
Item {
    BackButton {}
    Text {
        id: text
        text: qsTr("Cleaning")
        color: "darkgray"
        font.pixelSize: 80
        anchors.centerIn: parent
    }
}
